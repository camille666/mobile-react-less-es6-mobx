!(function(doc, win) {
    var docEl = doc.documentElement,
        resizeEvt =
            'orientationchange' in window ? 'orientationchange' : 'resize',
        reCalc = function() {
            var clientWidth = docEl.clientWidth
            if (!clientWidth) return
            docEl.style.fontSize = 28 * (clientWidth / 375) + 'px'
        }
    if (!doc.addEventListener) return
    win.addEventListener(resizeEvt, reCalc, false)
    doc.addEventListener('DOMContentLoaded', reCalc, false)
})(document, window)
