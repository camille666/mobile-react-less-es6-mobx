import React from 'react'
import { NavLink, Route, Switch } from 'react-router-dom'
import ExampleA from './exampleA/index'
import ExampleB from './exampleB/index'
import styles from './index.less'

export default class HomePage extends React.Component {
    render() {
        return (
            <div className={styles.homeBox}>
                <div className={styles.navBox}>
                    <NavLink
                        to="/home/exampleA"
                        activeClassName={styles.linkActive}
                    >
                        测测你适合养什么宠物
                    </NavLink>
                    <NavLink
                        to="/home/exampleB"
                        activeClassName={styles.linkActive}
                    >
                        七夕买房活动
                    </NavLink>
                </div>
                <div className={styles.pageBox}>
                    <Switch>
                        <Route
                            exact={true}
                            path="/home/exampleA"
                            component={ExampleA}
                        />
                        <Route
                            exact={true}
                            path="/home/exampleB"
                            component={ExampleB}
                        />
                    </Switch>
                </div>
            </div>
        )
    }
}
