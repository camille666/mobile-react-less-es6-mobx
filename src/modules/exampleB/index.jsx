import React, { Component } from 'react'
import axios from 'axios'
import styles from './index.less'

export default class ExampleB extends Component {
    state = {
        detailB: ''
    }

    componentDidMount() {
        const that = this

        axios.get('/api/detailB').then(function(response) {
            that.setState({
                detailB: response.data.data.text
            })
        })
    }

    render() {
        return <div className={styles.contentB}>b:{this.state.detailB}</div>
    }
}
