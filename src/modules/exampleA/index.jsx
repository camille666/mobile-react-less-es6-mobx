import React, { Component } from 'react'
import axios from 'axios'
import styles from './index.less'

export default class ExampleA extends Component {
    state = {
        detailA: ''
    }

    componentDidMount() {
        const that = this
        axios.get('/api/detailA').then(function(response) {
            that.setState({
                detailA: response.data.data.text
            })
        })
    }

    render() {
        return <div className={styles.contentA}>{this.state.detailA}</div>
    }
}
