const paths = require('./paths.js')
const path = require('path')
const ROOT_PATH = paths.ROOT_PATH
const OUTPUT_PATH = paths.OUTPUT_PATH
const SOURCE_PATH = paths.SOURCE_PATH
const OUTPUT_ASSETS_PATH = paths.OUTPUT_ASSETS_PATH

const CopyWebpackPlugin = require('copy-webpack-plugin')

// 在编译之前，把旧的编译文件清空，否则会持续生成不同hash的文件，我们只需要保留最新编译的文件。
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

// 入口文件配置
const ENTRIES_CONFIG = {
    main: SOURCE_PATH + '/app.jsx'
}

// 输出配置
const OUTPUT_CONFIG = {
    publicPath: '/',
    path: OUTPUT_PATH,
    filename: '[name].bundle.js',
    chunkFilename: '[name].[chunkHash:5].chunk.js'
}

// 处理的文件类型和范围
const RESOLVE_CONFIG = {
    extensions: ['.jsx', '.js'],
    alias: {
        styles: path.resolve(SOURCE_PATH, 'assets/styles'),
        js: path.resolve(SOURCE_PATH, 'assets/js'),
        fonts: path.resolve(SOURCE_PATH, 'assets/font'),
        src: path.resolve(SOURCE_PATH),
        components: path.resolve(SOURCE_PATH, 'components'),
        modules: path.resolve(SOURCE_PATH, 'modules')
    }
}

// 备份资源插件
const copyWebpackPlugin = new CopyWebpackPlugin([
    // 复制所有当前工程的 assets 到 output 目录
    {
        context: SOURCE_PATH,
        from: 'assets/',
        to: OUTPUT_ASSETS_PATH + '/'
    }
])

// 插件
const PLUGINS = [new CleanWebpackPlugin(), copyWebpackPlugin]

// 加载器
const MODULE_RULES = [
    {
        // 前置(在执行编译之前去执行eslint-loader检查代码规范，有报错就不执行编译)
        enforce: 'pre',
        test: /.(js|jsx)$/,
        loader: 'eslint-loader',
        exclude: [path.join(ROOT_PATH, './node_modules')],
        options: {
            fix: true
        }
    },
    {
        // 将jsx转换成 js
        test: /.jsx$/,
        loader: 'babel-loader'
    },
    {
        // 将ES6语法转成 低版本语法
        test: /.js$/,
        loader: 'babel-loader',
        exclude: [
            // 排除node_modules 下的js
            path.join(ROOT_PATH, './node_modules')
        ]
    },
    {
        test: /\.(ttf|eot|svg|png|gif|jpg|jpeg|swf)$/,
        exclude: /node_modules/,
        include: /assets/,
        loader: 'file-loader'
    }
]

module.exports = {
    entry: ENTRIES_CONFIG,
    output: OUTPUT_CONFIG,
    resolve: RESOLVE_CONFIG,
    plugins: PLUGINS,
    module: { rules: MODULE_RULES }
}
